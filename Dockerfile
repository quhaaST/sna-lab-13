FROM python:3.6

WORKDIR /app

COPY . ./app

# For some other command
CMD ["python", "main.py"]
