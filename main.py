class MathOperations:
    def __init__(self):
        pass

    def add(self, x, y):
        return x + y

    def multiply(self, x, y):
        return x * y

if __name__ == "__main__":
    math_operations = MathOperations()
    print("Math operations block created!")
