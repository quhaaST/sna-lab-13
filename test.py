import unittest
from main import MathOperations

class TestOperations(unittest.TestCase):
    def setUp(self):
        self.math_operations = MathOperations()
    
    def test_add(self):
        self.assertEqual(self.math_operations.add(4,7), 11)
    
    def test_multiply(self):
        self.assertEqual(self.math_operations.multiply(3,7), 21)

if __name__ == "__main__":
    unittest.main()
